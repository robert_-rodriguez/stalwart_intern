import json
import os

QUOTES_AND_WHITESPACE = ' \t\n\r"\''


def validate_config(config, scp_location='./policies/'):
    accounts = [account_number.strip(QUOTES_AND_WHITESPACE) for key in config for account_number in str(key).split(',')]

    problems = {
        **validate_account_numbers(accounts),
        **check_for_duplicate_account_numbers(accounts),
        **validate_scps_exist(config, scp_location),
        **validate_scp_assignments(config)
    }

    if problems:
        raise ValueError('\n' + '\n'.join(key + ': ' + problems[key] for key in problems))


def validate_account_numbers(accounts):
    problems = {}

    bad_length_numbers = []
    bad_character_numbers = []
    for number in accounts:
        if len(number) != 12:
            bad_length_numbers.append(number)
        if not number.isnumeric():
            bad_character_numbers.append(number)

    if bad_length_numbers:
        problems['Bad length account numbers'] = str(set(bad_length_numbers))
    if bad_character_numbers:
        problems['Account numbers with bad characters'] = str(set(bad_character_numbers))

    return problems


def check_for_duplicate_account_numbers(accounts):
    problems = {}

    if len(accounts) != len(set(accounts)):
        problems['Duplicate account numbers'] = str(set(num for num in accounts if accounts.count(num) > 1))

    return problems


def validate_scps_exist(config, scp_location):
    if not os.path.isdir(scp_location):
        return {'Provided SCP location does not exist or is not a directory': scp_location}
    if not os.listdir(scp_location):
        return {'Provided SCP location is empty': scp_location}

    referenced_scps = [scp for key in config for scp in config[key]]
    existing_scps = [os.path.splitext(os.path.basename(file_name))[0] for file_name in os.listdir(scp_location) if file_name.endswith('.json')]
    missing_scps = [scp for scp in referenced_scps if scp not in existing_scps]

    if missing_scps:
        return {'Referenced scps do not exist in scp folder': str(set(missing_scps))}
    return {}


def validate_scp_assignments(config):
    problems = {}

    duplicate_scps = []
    for key, scp_list in config.items():
        if len(scp_list) != len(set(scp_list)):
            duplicate_scps.append(key)

    if duplicate_scps:
        problems['Account sets with duplicate scps'] = str(duplicate_scps)

    return problems


def validate_scps(config, scp_location='./policies/'):
    referenced_scps = [scp.strip(QUOTES_AND_WHITESPACE) for key in config for scp in config[key]]

    for scp in referenced_scps:
        validate_scp(os.path.join(scp_location, scp + '.json'))


def validate_scp(file_name):
    policy = None

    with open(file_name) as f:
        try:
            policy = json.load(f)
        except json.decoder.JSONDecodeError:
            raise ValueError('SCP file is not properly formated json: ' + file_name)

    assert 'Version' in policy
    assert 'Statement' in policy

    if not isinstance(policy['Statement'], list):
        policy['Statement'] = [policy['Statement']]

    for statement in policy['Statement']:
        assert 'Resource' in statement
        assert 'Effect' in statement
        if statement['Effect'] == 'Allow':
            assert 'Action' in statement
        else:
            assert 'Action' in statement or 'NotAction' in statement
