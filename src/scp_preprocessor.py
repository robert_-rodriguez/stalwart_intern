#!/bin/python3

import argparse
import json
import os
import yaml

from src import validation

QUOTES_AND_WHITESPACE = ' \t\n\r"\''


def create_merged_scps(config, scp_folder='./policies'):
    output_dir = prepare_scp_output_folder(scp_folder)

    i = 0
    for account_set in sorted(config):
        i += 1
        merged_scp = {}
        for scp in config[account_set]:
            with open(os.path.join(scp_folder, scp + '.json')) as f:
                if not merged_scp:
                    merged_scp = json.load(f)
                merged_scp['Statement'] += list(json.load(f)['Statment'])

        merged_scp['Statement'] = list(set(merged_scp['Statement']))  # THIS WON'T WORK

        with open(os.path.join(output_dir, str(i) + '.json'), 'w') as f:
            json.dump(merged_scp, f)


def prepare_scp_output_folder(scp_folder):
    output_dir = os.path.join(scp_folder, '../output_policies/')
    os.makedirs(output_dir, exist_ok=True)

    for file_name in os.listdir(output_dir):
        if os.path.isfile(os.path.join(output_dir, file_name)):
            os.unlink(os.path.join(output_dir, file_name))
    return output_dir


def group_account_sets(config):
    separated_accounts = {}

    for account_set in config:
        for account_number in str(account_set).strip().split(','):
            account_number = account_number.strip(QUOTES_AND_WHITESPACE)

            separated_accounts[account_number] = sorted(str(scp).strip(QUOTES_AND_WHITESPACE) for scp in config[account_set])

    grouped_config = {}

    while separated_accounts:
        first_key = next(iter(separated_accounts))
        first_value = separated_accounts[first_key]
        keys_with_same_values = [first_key]

        for key, value in separated_accounts.items():
            if key != first_key and value == first_value:
                keys_with_same_values.append(key)

        grouped_config[','.join(keys_with_same_values)] = first_value
        for key in keys_with_same_values:
            del separated_accounts[key]

    return grouped_config


def create_ous(config):
    pass


def get_configuration_from_file(file_location='./cfg/accounts.yml'):
    if not os.path.isfile(file_location):
        raise FileNotFoundError('Provided config file location is not a file: ' + file_location)

    with open(file_location) as f:
        return yaml.load(f)


if __name__ == "__main__":
    config = get_configuration_from_file()
    validation.validate_config(config)
    validation.validate_scps(config)

    config = group_account_sets(config)
